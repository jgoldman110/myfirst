FX Trading Application

Background 
Forex, also known as foreign exchange, FX or currency trading, is a decentralized global market where all the world's currencies trade. The forex market is the largest, most liquid market in the world with an average daily trading volume exceeding $5 trillion, bigger than all the combined stock markets.

Just like stocks, you can trade currency based on what you think its value is (or where it's headed). But the big difference with forex is that you can trade up or down just as easily. If you think a currency will increase in value, you can buy it. If you think it will decrease, you can sell it. With a market this large, finding a buyer when you're selling and a seller when you're buying is much easier than in in other markets. 

Maybe you hear on the news that China is devaluing its currency to draw more foreign business into its country. If you think that trend will continue, you could make a forex trade by selling the Chinese currency against another currency, say, the US dollar. The more the Chinese currency devalues against the US dollar, the higher your profits. If the Chinese currency increases in value while you have your sell position open, then your losses increase, and you want to get out of the trade.

All forex trades involve two currencies because you're betting on the value of a currency against another. Think of EUR/USD, the most-traded currency pair in the world. EUR, the first currency in the pair, is the base, and USD, the second, is the counter. When you see a price quoted on your platform, that price is how much one euro is worth in US dollars. You always see two prices because one is the buy price and one is the sell. The difference between the two is the spread. When you click buy or sell, you are buying or selling the first currency in the pair.


Objective
Working in your project team you will design, build and deploy an FX trading application to be used by clients, traders and risk managers. 

Application Users:
Trader is a person who prices one/more currency pairs. The trader should be able to view the trades whose currency pairs are priced by him.
Client is the end user who books a trade with the Bank. The client will see the price by the offered by trader and can book the FX trade at that price. 
Risk manager is a senior officer in the bank who monitors the trade details of all the traders. 

Core Functionality
Your application should support the following features:	
User management: Add/modify/delete the list of traders, clients and risk manager.
Pricing: Where the trader can price the currencies
Trade Entry: Where the client can book deal and view the deal blotter (list view of their deals) 
Trade Summary: Where the trader can view all his currency-based deals booked by the clients 


Additional Requirements

Pages
You are expected to design & develop the different pages like trader profile maintenance, trader currency pricing, client deal booking, deal blotters for client and trader, blotter for risk manager

Users
The application can have N traders, M clients and 1 risk manager.

The system should:
Have reasonable performance
Be flexible in the information that it displays


Project Schedule

Phase 1 – Analysis and Design
The analysts should develop a high-level system design that breaks the application into components layers and defines contracts for interaction between them.

Phase 2 – Component Development
The analysts should focus on development and unit testing of the core components 

Phase 3 – System Integration and Testing 
During this phase the teams should work to integrate the key components and complete full system integration testing.  They should also prepare their project presentation and technical demonstration.

Phase 4 – Project Fair
During the final afternoon of the program the analysts will be 
Expected to present their final projects to their colleagues and managers.
Formally assessed by small teams of instructors and managers. Assessment criteria are outlined in a separate document.  


FX trading application – Functional Requirements

Requirements:
Application should contain the following screens.
Login Screen
Trader profile maintenance
Deal pricing screen
Trade capture screen
Trader blotter
Risk manager blotter
Login Screen:
The client, traders and risk manager login and entitlements should be managed.
The client should have access to the deal booking screen only
The trader should have access to the deal pricing screen and trader blotter only
The risk manager should have access to the trader profile maintenance and Risk manager blotter only. 
 
Trader profile maintenance
The risk manager should be able to retrieve the traders and assign the set of currency pairs to them. A currency pair cannot be assigned to more than one trader.

Deal pricing screen:
The currency pairs assigned to him should be listed to him and he should be able to provide price to him. All the currency pairs will be in terms of USD.

Trade capture screen:
The FX deal would consist of the following fields to be provided by the client.
Currency pair
Dealing currency
Indicator (Buy dealing currency/sell dealing currency)
Dealing amount
Trade date (current date)
Value date
Price (the trader provided price should be populated based on the currency pair selected)


Trader blotter:
There should be a page where the trader should be able to see the details of the trades whose currency pairs are priced.

Risk manager blotter:
The risk manager should have a deal blotter to see all the deals booked. There can be filters to view based on the trader name / currency pair.

Charting:
To assist the traders a web page that displays line charts for some currency pairs that the user can choose from a dropdown list (or menu) is desirable. This could also be restricted to time slots the user can select.  This data is easy to get, for example you could download excel files from here (http://www.global-view.com/forex-trading-tools/forex-history/) save the excel data as a csv and populate a database table from that data.

Technical Requirements:
Spring Boot MVC Web Application
UI Interface	    	:  HTML5, CSS3 and Thymeleaf Templates
Database	    		:  MySQL
Architecture	    	:  MVC (UI  business logic data layer)



Remember the following best practices:

Work as a team, play to your individual strengths and allocate project tasks across all team members.
Work as a team, constantly communicating and avoiding any “solo runs”. Agile practices such as regular (at least daily) stand-ups will help you remain on track.
Spend an appropriate amount of time on the analysis & design phase. This will help you all agree on what is to be built.
Build a “minimal viable product” as quickly as possible with a simple UI, basic business logic and a database.




